import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Exercitiul2 {
    public static void main(String[] args) {
        new GUI();
    }
}

class GUI extends JFrame {
    JTextField jTextField1;
    JTextField jTextField2;
    JTextField jTextField3;
    JButton button = new JButton("Calculeaza");

    GUI() {
        this.setTitle("Exercitiul 2");
        this.setSize(150, 200);
        init();
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        jTextField1 = new JTextField();
        jTextField1.setBounds(20, 20, 100, 20);

        jTextField2 = new JTextField();
        jTextField2.setBounds(20, 50, 100, 20);

        button.setBounds(20, 80, 100, 20);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    int a = Integer.parseInt(jTextField1.getText());
                    int b = Integer.parseInt(jTextField2.getText());
                    jTextField3.setText(a+b+"");
                }catch (NumberFormatException ignored) {
                    jTextField3.setText("Input invalid");
                }
            }
        });

        jTextField3 = new JTextField();
        jTextField3.setEditable(false);
        jTextField3.setBounds(20, 110, 100, 20);

        this.add(jTextField1);
        this.add(jTextField2);
        this.add(jTextField3);
        this.add(button);
    }
}